run_once("xscreensaver", "-no-splash")         -- starts screensaver daemon 
run_once("xsetroot", "-cursor_name left_ptr")  -- sets the cursor icon
--run_once("bash /home/nick/.screenlayout/gud.sh")         -- starts screensaver daemon 
run_once("owncloud")  -- sets the cursor icon
run_once("dropbox start")
run_once("wicd-gtk")
run_once("nitrogen --restore")
